SARTEL Clément									       IESE5

ESCALLIER Mathis

**SHOT-Alarm**

***IOT : PROJET LORA***

**Lien gitlab : **[https://gitlab.com/projet-lora/RIOT/-/tree/master/tests/project-lora](https://gitlab.com/projet-lora/RIOT/-/tree/master/tests/project-lora) 

**Raport au format PDF :**[https://drive.google.com/file/d/1qFI3ogCh_Tt-J-e4h4pey8bbqHpmBs7d/view](https://drive.google.com/file/d/1qFI3ogCh_Tt-J-e4h4pey8bbqHpmBs7d/view)

**Présentation (google slides) :**[ https://docs.google.com/presentation](https://docs.google.com/presentation/d/1nzADBjMVOk2pW9frapncjLsKqM1pW0oo4p845QcYjyw/edit?usp=sharing)

**Vidéo démonstration :** [video](https://drive.google.com/file/d/1tphaaBnzPVsPPADxUrIxIHqlNWtHG-Fj/view?usp=sharing)

**Nombre d'heures passées (en dehors des cours) : 20h/personnes**

![](Aspose.Words.8809e566-5ed2-4313-91c7-79a92b8d9ef8.001.jpeg)

![](Aspose.Words.8809e566-5ed2-4313-91c7-79a92b8d9ef8.002.png)

**Professeurs :** Didier DONSEZ et Francois-Xavier MOLINA




# 1 - Description du produit

Le système SHOT-Alarm est conçu afin de protéger votre domicile et ralentir/capturer les malfrats en cas d’intrusion, mais aussi de prévenir en cas d'incendie dans la pièce.

Grâce à ses différents capteurs, le boîtier est capable de détecter l’intrusion ou la présence d’une personne dans votre domicile. Les différents actionneurs connectés peuvent actionner différents systèmes de dissuasion/capture. Enfin, l’utilisation de leds et buzzer permet d’alerter/dissuader rapidement. 

Des capteurs de CO2, de température et d’humidité permettent de détecter un feu dans la pièce où se trouve notre alarme, puis d’envoyer une alerte.

Le système de base contient les composants/fonctionnalités ci-dessous, bien-sûr vous pourrez améliorer votre alarme avec différentes options en fonction de vos attentes. 

# 2 - Architecture matérielle

Dans cette partie, nous allons décrire l’architecture matérielle de notre système.

# 2.1 - Composants utilisés

**2.1 Cartes électroniques** 

LoRa-E5 Dev Board :

*Permet l’activation et la communication avec les capteurs/moteurs et assure la communication Lora*

STM32F446RE (loader) :

*Permet de charger la carte lora via le port SWD (cette carte est uniquement utilisée pour le développement de l'alarme)*



**2.2 Capteurs**



PIR sensor :

![](Aspose.Words.8809e566-5ed2-4313-91c7-79a92b8d9ef8.003.png)

<center>Capteur de détection de mouvements</center>

SCD30 sensor :

![](Aspose.Words.8809e566-5ed2-4313-91c7-79a92b8d9ef8.004.png)

<center>Capteur de CO2/Humidité/Température</center>



**2.3 Actionneurs**

Servo-moteur :

![](Aspose.Words.8809e566-5ed2-4313-91c7-79a92b8d9ef8.005.png)

<center>Déclenche le “shot” (tir de pistolet nerf)</center>

Bouton :

![](Aspose.Words.8809e566-5ed2-4313-91c7-79a92b8d9ef8.006.png)

<center>Contrôle l’état de l’alarme</center>

Led :

![](Aspose.Words.8809e566-5ed2-4313-91c7-79a92b8d9ef8.007.png)

<center>Indication lumineuse de l’état de l’alarme et avertisseur lumineux</center>

Buzzer :

![](Aspose.Words.8809e566-5ed2-4313-91c7-79a92b8d9ef8.008.png)

<center>Avertisseur sonore de l’état de l’alarme</center>

# 2.2 - Connexions des composants

Nous pouvons observer ci-dessous le schéma de connexion de nos différents composants sur la carte de développement Lora E5. 

![](Aspose.Words.8809e566-5ed2-4313-91c7-79a92b8d9ef8.009.png)
# 3 - Programmation

# 3.1 - Programme Principal

Décrivons le programme principal. Le système d’alarme possède trois états :

POWER OFF : le système est éteint, rien ne se passe (état du reset), attente d’appuie sur le bouton

POWER ON : le système après avoir démarré (musique de démarrage + allumage de la led) réalise une routine qui consiste à tester les différents capteurs de la carte et envoyer certaines données sur internet (via l’outil Cayenne). Si un capteur détecte un comportement anormal, on passe à l'état d'alarme.

ALARM ON : l’alarme est déclenchée, la led clignote et le buzzer produit un son d’alarme. Il faut maintenir le bouton pour éteindre l’alarme et repasser au mode OFF.

Nous pouvons observer ci-dessous l'algorithme du fonctionnement de notre alarme.

![](Aspose.Words.8809e566-5ed2-4313-91c7-79a92b8d9ef8.010.png)

<center>Algorigramme du programme</center>

## 3.2 - Programmation des composants

***PIR sensor***

*référence : <https://wiki.seeedstudio.com/Grove-PIR_Motion_Sensor/>*

Le PIR sensor est un composant permettant de détecter le mouvement d’un être vivant, nous l’avons implémenté dans le programme par les librairies “pir.h” et “pir\_params.h” disponible dans le dossier RIOT.

Afin d’obtenir les valeurs du capteur, il suffit de mettre un pin en mode entrée, le capteur alimenté renvoie alors “0” si aucun mouvement n’est détecté ou “1” dans le cas contraire.

Nous nous servons de ce capteur pour détecter une intrusion, si un mouvement est détecté, le tir de nerf est déclenché et l’alarme se met en route.

***SCD30 sensor***

*référence : <https://media.digikey.com/pdf/Data%20Sheets/Seeed%20Technology/SCD30_Web.pdf>*

Le SCD30 sensor est un capteur qui fonctionne en communication I2C, nous utilisons les librairies RIOT “scd30.h”, “scd30\_params.h” et “scd30\_internal.h” afin d’initialiser et configurer facilement ce capteur.

Différentes fonctions (disponibles dans le code) nous permettent de relever la température, la concentration de CO2 ainsi que le taux d’humidité.

Ce capteur nous permet de détecter un incendie en fonction de l’évolution de la température, une pollution anormale en fonction du taux de CO2 et une inondation en fonction du taux d’humidité. Nous nous en servons également pour mesurer une augmentation anormale de la température ou de CO2 qui pourrait être lié à l’intrusion d’un inconnu qui n’aurait pas déclenché le capteur de mouvement.

***Buzzer***

*référence : <https://www.farnell.com/datasheets/2891582.pdf> (exemple)*

Le buzzer nous permet d’émettre principalement le son de l’alarme en cas de détection d’intrusion. Il est commandé par un signal PWM dont nous faisons varier la fréquence afin d’obtenir des sons différents (voir code).

Nous avons également ajouté un son pour l’allumage de l’alarme et un autre son pour le reset de l’alarme. 

***Led***

*référence : <https://www.farnell.com/datasheets/3360919.pdf> (exemple)*

La led nous permet par la commande d’un GPIO en output d’émettre un signal lumineux lors de la sonnerie de l’alarme. Nous pourrions également nous en servir pour indiquer dans quel état se trouve l’alarme.

***Servo-moteur***

*référence : <https://hobbyking.com/fr_fr/towerpro-mg996r-10kg-servo-10kg-0-20sec-55g.html> (exemple)*

Le servo-moteur a pour fonction de déclencher le tir de nerf, nous le commandons par un signal PWM de fréquence 50 Hz sur lequel nous changeons le duty cycle pour choisir l’angle de positionnement. Nous avons construit ce signal PWM en commandant l’alternance de l’état d’un GPIO en output sur une fréquence de 50 Hz.

***Bouton***

*référence : <https://www.gotronic.fr/art-module-bouton-grove-111020000-19010.htm>*

Le bouton nous permet de commander l’alarme, il suffit de mettre un GPIO en mode entrée et de lire la valeur. Des boutons sont intégrés à la carte mais de par la construction de notre boîtier ils sont difficiles d’accès, c’est pourquoi nous utilisons un bouton externe pour allumer/reset l’alarme.

## 3.3 - Interface Réseau

## 3.3.1 - Communication Lora
![](Aspose.Words.8809e566-5ed2-4313-91c7-79a92b8d9ef8.011.png)

LoRa, de Long Range, est une technologie Radio LPWAN. Cette techno est brevetée par Semtech (fabricant de chip Radio) et elle est entièrement dédiée au monde de l’IoT.

Le LoRaWAN quant à lui est un protocole de télécommunication radio permettant la communication à bas débit d'objets connectés. En France, il se retrouve sur la bande de fréquence 868 mégahertz.

Nous allons donc nous servir de la technologie LoRa pour envoyer nos mesures de capteur directement sur internet, pour qu'elles soient accessibles de n’importe où. Pour cela nous allons utiliser l’outil Cayenne.

Cayenne Low Power Payload (LPP) offre un moyen pratique et facile d'envoyer des données sur des réseaux LPWAN tels que LoRaWAN. Cayenne LPP est conforme à la restriction de taille de charge utile, qui peut être réduite à 11 octets, et permet à l'appareil d'envoyer plusieurs données de capteur en même temps.

![](Aspose.Words.8809e566-5ed2-4313-91c7-79a92b8d9ef8.012.png)

## 3.3.2 - Interface Web Cayenne

Pour pouvoir visualiser les mesures de notre alarme nous utilisons donc le site de Cayenne. Pour cela, nous devons envoyer nos données via différents canaux de communication. La liste des canaux et les valeurs correspondantes sont les suivantes :

- 0 : TEMP\_value de type float
- 1 : intrusion de type uint8\_t
- 2 : gun\_state de type uint8\_t
- 3 : alarm\_state de type uint8\_t
- 4 : HUMIDITY\_value de type float
- 5 : CO2\_value de type float
- 6 : fire\_detection de type uint8\_t
- 7 : pollution\_detection de type uint8\_t
- 8 : flood\_detection de type uint8\_t

Une fois que notre alarme envoie les valeurs de nos variables, nous pouvons retrouver les mesures sur l’outil en ligne Cayenne. L’affichage des résultats est visible sur les figures ci-dessous.

![](Aspose.Words.8809e566-5ed2-4313-91c7-79a92b8d9ef8.013.png)

<center>Alarme armé</center>

![](Aspose.Words.8809e566-5ed2-4313-91c7-79a92b8d9ef8.013.png)

<center>Alarme déclenché (exemple : mouvement détection)</center> 



# 4 - Produit final

Abordons le produit final, après modélisation et impression d’une boite en 3D, la mise en place du nerf avec un servo moteur, de la carte Lora et des différents capteurs, nous obtenons la SHOT-Alarm!

![](Aspose.Words.8809e566-5ed2-4313-91c7-79a92b8d9ef8.014.jpeg)![](Aspose.Words.8809e566-5ed2-4313-91c7-79a92b8d9ef8.015.jpeg)

***Photos de la SHOT-Alarm***

**Lien gitlab : **[https://gitlab.com/projet-lora/RIOT/-/tree/master/tests/project-lora](https://gitlab.com/projet-lora/RIOT/-/tree/master/tests/project-lora) 

**Vidéo démonstration :** [video](https://drive.google.com/file/d/1tphaaBnzPVsPPADxUrIxIHqlNWtHG-Fj/view?usp=sharing)

Nous allons désormais décrire comment nous pourrions développer notre système pour le commercialiser.


## 4.1 - Architecture globale du réseau de sirènes d’alarme

Il faudrait dans un premier temps créer un réseau d’alarmes. C’est-à-dire créer d’autres SHOT-Alarm et les connecter entre elles via la communication Lora. Si une alarme détecte une intrusion/événement, toutes les alarmes pourraient se mettre à sonner (par exemple).

## 4.2 - Définir la sécurité globale (clé de chiffrage)

Une fois la communication réseau établie, il faut bien évidemment crypter les données afin de rendre le système difficilement hackable. 

Nous pourrions utiliser des fonctions/bibliothèques de chiffrement. 

## 4.3 - Définir l’architecture matérielle de l’objet

Voir section 2.

## 4.4 - Estimer le coût de la BOM de votre produit (composants, PCB et enclosure) pour 5000 unités produites
|**Matériel**|**Quantité**|**Prix unitaire (€)**|
| :-: | :-: | :-: |
|**LoRa-E5 Dev Board**|**1**|**19,2**|
|**PIR sensor**|**1**|**5,4**|
|**SCD30 sensor**|**1**|**65,66**|
|**Servo-moteur**|**1**|**14,56**|
|**Bouton**|**1**|**2,2**|
|**Led**|**1**|**0,1**|
|**Buzzer**|**1**|**0,17**|
|**Nerf**|**1**|**5,99**|
|**Boite 3D**|**1**|**1**|
|**TOTAL**||**114,28**|

Au total, notre système d’alarme coûterait environ 120€ en termes de composants. Dans une démarche de commercialisation, il faudrait penser au prix de production et autres (assemblage, impression, développement, distribution, communication, vente en ligne, licences, etc.) afin de pouvoir fixer un prix ajusté à toutes ces variables.

Ce prix est assujetti à changer de par les évolutions de la version de l’alarme (celle-ci étant un prototype).
## 4.5 - Estimer le coût de certification ETSI du produit, le coût de [certification LoRa Alliance](https://lora-alliance.org/lorawan-certification/) du produit …

Voici les dépense liées aux différentes certifications que nous devons prendre en compte :

- Certification ETSI : 300€ par an
- Certification LoRa Alliance 10000$ par an

## 4.6 - Proposer une implémentation du logiciel embarqué de l’objet défini.

Le logiciel embarqué sera implémenté sur les cartes Lora E5, nous pouvons également fabriquer d’autres cartes électroniques basées sur le module “LoRa-E5 STM32WLE5JC” afin de gagner en coût de production. Nous pouvons également penser à développer un système permettant de mettre à jour le firmware de nos alarmes à distance par la communication Lora. 

## 4.7 - Définir le format LPP des messages LoRaWAN uplink et downlink

Ce prototype ne contient pas de messages LoRaWAN downlink, en effet notre alarme permet d’envoyer les données à l’interface web Cayenne (uplink) mais elle ne reçoit pas de données (si ce n’est pour l’initialisation de la connexion). 

Le format des données uplink est disponible section 3.3.2.

## 4.8 - Définir le logiciel embarqué de l’objet sirène

Le logiciel embarqué de l’objet sirène est défini par une fonction “alarme” sur laquelle nous pouvons choisir la durée en paramètre. Cette fonction est disponible dans le code. La fréquence des sons de l’alarme varie entre 2000 Hz et 3000 Hz qui sont des sons aigus et désagréables pour l’audition humaine (fonction recherchée pour une alarme).

## 4.9 - Donner les métriques logiciel du logiciel embarqué (nombre de lignes de code, taille du binaire du firmware ie le fichier .bin)

**Nombre de ligne de code : 623**

**Taille du fichier binaire (.bin) : 1,8 Mo (cayenne.elf)**

Bien évidemment, cette version étant un prototype, le code peut être fortement optimisé et nous pouvons réduire considérablement ces 2 valeurs.

## 4.10 - Montrer les changements de comportement de l’objet en fonction des événements (normal, incident détecté, retour à la normal).

Voir section 3.1


## 4.11 - Estimer la durée de vie de la batterie en classe A, en classe B et en classe C 
En utilisant l'outil  <https://www.elsys.se/en/battery-life-calculator/> on fixe :

Capacité de notre batterie : 2500 mAh

Sample time : 2 minutes par envoi

Spreading factor 80%

![](Aspose.Words.8809e566-5ed2-4313-91c7-79a92b8d9ef8.016.png)

D’après cet outil, la durée de vie de notre batterie est donc de 1,6 ans, ce qui est relativement peu. On s'aperçoit que la transmission et les mesures du capteur de Co2 sont les événements les plus énergivores.

Notre analyse a ses limites. En effet, elle utilise des valeurs de consommation de capteurs par défaut, les nôtres peuvent donc être différentes. De plus, il n'était pas possible de prendre en compte les consommations du servomoteur, du buzzer et de la leds. Notre durée de vie réelle est donc probablement inférieure à 1,6 ans.

## 4.12 - Réaliser une analyse (briève) du cycle de vie du produit ([ACV](https://fr.wikipedia.org/wiki/Analyse_du_cycle_de_vie))

Nous définissons le cycle de vie de notre produit comme on le voit sur la figure ci-dessous.

![](Aspose.Words.8809e566-5ed2-4313-91c7-79a92b8d9ef8.017.png)

<center>Cycle de vie de notre produit</center>

Nous avons dans un premier temps regardé les besoins du marché. Nous sommes les seuls sur le marché qui proposons un système d'appréhension des malfrats.

Ensuite nous avons conçu rapidement notre produit pour respecter un time to market relativement faible et ainsi nous imposer directement sur le marché. 

Une fois vendu nous avons des techniciens qui viennent installer le système d’alarme et vérifier son bon fonctionnement.

Nous fournissons également avec notre alarme un SAV sur 10 ans. Cette période n’est pas prolongeable car l’idée est d'arrêter de la commercialiser lorsqu’une version améliorée aura été développée en utilisant les retours utilisateurs.

L’objectif est de sortir un nouveau modèle tous les 3 ou 4 ans.

## 4.13 - Recherche et analyse des produits concurrents

Nous avons choisi de comparer notre alarme avec celle présente sur le marché. Pour cela nous avons sélectionné 4 alternatives différentes.

Dans un premier temps nous allons identifier les différentes fonctionnalités et les comparer. Ci-dessous se trouve un tableau récapitulatif des différentes fonctionnalités.



||Détection Inondation|Détection Incendie|Détection Intrusion|
| :- | :- | :- | :- |
|[Avertisseur vocal connecté 126 dB Radio LoRa](https://www.aet.fr/produit/avertisseur-vocal-connecte-126-db-radio-lora-xpr901111.html) |Non|Non|Non|
|[R602A LoRaWAN Wireless Siren](https://iqflow.io/products/r602a-lorawan-wireless-siren) |Non|Non|Non|
|[Alarme autonome ineo-sense ACS Switch Buzz](https://www.dataprint.fr/solutions-m2m-iot/alarme-autonome-ineo-sense-acs-switch-buzz) |Non|Non|Oui|
|[MClimate CO2 Sensor and Notifier LoRaWAN](https://docs.mclimate.eu/mclimate-lorawan-devices/devices/mclimate-co2-sensor-and-notifier-lorawan) |Non|Oui|Non|
|Shot-Alarm|Oui|Oui|Oui|

Nous voyons donc bien que notre alarme est la seule du marché à proposer autant de fonctionnalités. Nous allons maintenant comparer leurs caractéristiques telles que la puissance sonore, la tension d’alimentation ou encore le prix.

Ci-dessous	se trouve le tableau récapitulatif des différents critères.	 



||Puissance sonore en dB|leds|Tension en V|batterie fournies|batterie life en année|Prix en €|
| :- | :- | :- | :- | :- | :- | :- |
|[Avertisseur vocal connecté 126 dB Radio LoRa](https://www.aet.fr/produit/avertisseur-vocal-connecte-126-db-radio-lora-xpr901111.html) |126|Non|115 / 230|Oui|?|?|
|[R602A LoRaWAN Wireless Siren](https://iqflow.io/products/r602a-lorawan-wireless-siren) |80|Oui|12|?|5|179|
|[Alarme autonome ineo-sense ACS Switch Buzz](https://www.dataprint.fr/solutions-m2m-iot/alarme-autonome-ineo-sense-acs-switch-buzz) |100|Oui|?|Oui|?|?|
|[MClimate CO2 Sensor and Notifier LoRaWAN](https://docs.mclimate.eu/mclimate-lorawan-devices/devices/mclimate-co2-sensor-and-notifier-lorawan) |80|Oui|3|Non|10|?|
|Shot-Alarm|80|Oui|3|Non|1,6|250|

Parmi les valeurs que nous avons réussi à obtenir, nous voyons que notre alarme est relativement semblable aux autres produits, mais avec plus de fonctionnalités. 

## 4.14 - Définir les solutions utilisables pour localiser l’objet sirène (à des fins d’inventaire)

Il existe des solutions nous permettant de localiser notre produit sans nécessiter l’utilisation d’un GPS. Parmis ces solutions nous en avons retenue deux.

**Géolocalisation TDOA** : La localisation d’une émission à l’aide du procédé TDOA (TimeDifference Of Arrival) doit remplir la même condition préalable que la goniométrie selon la méthode éprouvée Angle of Arrival (AOA) : des récepteurs doivent être présents en nombre suffisant dans la portée de l’émetteur recherché. Ses signaux, qui se propagent à une vitesse constante, arrivent aux récepteurs à des moments légèrement différents, car ces récepteurs sont en général situés à des distances différentes du lieu d’émission. À partir de ces différences de temps, il est alors possible de calculer les coordonnées de l’émetteur.

**Géolocalisation LoRa Edge** : La solution LoRa Edge LR1110 utilise les capacités de géolocalisation LoRa Cloud ™ de Semtech pour réduire considérablement la consommation d'énergie en déterminant l'emplacement des actifs dans un solveur basé sur le cloud.

Nous pourrions donc utiliser l’une de ces deux techniques pour localiser nos produits à des fins d’inventaire par exemple.

# **5 - Pistes d’améliorations possibles**

Les améliorations que nous pouvons apporter à notre projet sont :

- Réception/Émission des datas via une application mobile (notification, contrôle à distance)
- Un LCD permettant de naviguer dans un menu pour désactiver certaines fonctionnalités
- Possibilité de concevoir un réseau d’alarmes
- Géolocalisation des alarmes
- Système d’alimentation (batterie rechargeable, alimentation secteur intelligente)
- Un Haut parleur pour messages vocaux personnalisés

- Utiliser une arme plus puissante (Flash Ball / Tazer / Filet de capture / Grille de fermeture)
- Proposer des mises à jour firmware à distance (LoRa)

# **6 - Conclusion**

Nous avons trouvé ce projet très intéressant et formateur. Nous avons pu découvrir la communication Lora et ses applications diverses mais également concevoir le prototype d’un produit commercialisable. 

Ce projet était aussi l’occasion de découvrir de nouveaux capteurs que nous ne connaissions pas.

Même si notre alarme n’est pas terminée nous avons réussi à réaliser une étude poussée de notre produit et de cibler les améliorations que nous pourrions lui apporter. 

Ce projet nous a d’ailleurs apporté plusieurs idées de projets personnels dans lesquelles nous aimerions nous lancer.
